<?php

namespace vams\adminapiclient;

class ApiResponseHeaderBag implements \IteratorAggregate, \Countable
{

    /** @var array */
    private $headers = [];

    public function __construct(array $headers = [])
    {
        $this->add($headers);
    }

    public function has($key)
    {
        return array_key_exists($this->normalize($key), $this->headers);
    }

    public function get($key, $default = null, $first = true)
    {
        $key = $this->normalize($key);

        if (!array_key_exists($this->normalize($key), $this->headers)) {
            return $default;
        }

        return $first ? $this->headers[$key][0] : $this->headers[$key];
    }

    public function set($key, $values, $replace = true)
    {

        $values = array_values((array) $values);
        if ($values === []) {
            return;
        }

        $key = $this->normalize($key);

        if ($replace || !array_key_exists($key, $this->headers)) {
            $this->headers[$key] = $values;
        } else {
            $this->headers[$key] = array_merge($this->headers[$key], $values);
        }
    }

    public function add(array $headers)
    {
        foreach ($headers as $key => $values) {
            $this->set($key, $values);
        }
    }

    public function replace(array $headers)
    {
        $this->headers = [];
        $this->add($headers);
    }

    public function remove($key)
    {
        unset($this->headers[$this->normalize($key)]);
    }

    public function all()
    {
        return $this->headers;
    }

    public function get_cookies()
    {
        return $this->get('set-cookie', [], false);
    }

    public function count()
    {
        return count($this->headers);
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->headers);
    }

    protected function normalize($key)
    {
        return str_replace('_', '-', strtolower($key));
    }

}
