<?php

namespace vams\adminapiclient\security;

class Crypt
{

    // Options
    const NO_PREPEND_IV = 1;
    const RAW_DATA = 2;

    // Paddings
    const PKCS7_PADDING = 1;
    const NO_PADDING = 2;
    const ZERO_PADDING = 3;
    const ZERO_PADDING_EMPTY_BLOCK = 4;
    const AUTODETECT_PADDING = 4;

    public static function encrypt($data, $key, $options = 0, $iv = null, $padding = self::PKCS7_PADDING)
    {
        if ($iv === null) {
            $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('AES-256-CBC'));
        }

        $data = self::pad_string($data, self::cipher_block_size('AES-256-CBC'), $padding);

        $result = openssl_encrypt($data, 'AES-256-CBC', $key, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $iv);

        if (($options & self::NO_PREPEND_IV) === 0) {
            $result = $iv . $result;
        }

        if (($options & self::RAW_DATA) === 0) {
            $result = base64_encode($result);
        }

        return $result;
    }

    public static function decrypt($data, $key, $options = 0, $iv = null, $padding = self::PKCS7_PADDING)
    {
        if (($options & self::RAW_DATA) === 0) {
            $data = base64_decode($data, true);

            if ($data === false) {
                return false;
            }
        }

        if (($options & self::NO_PREPEND_IV) === 0) {
            $iv_length = openssl_cipher_iv_length('AES-256-CBC');

            if (strlen($data) < $iv_length) {
                return false;
            }

            $iv = substr($data, 0, $iv_length);
            $data = substr($data, $iv_length);
        }

        $result = openssl_decrypt($data, 'AES-256-CBC', $key, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $iv);

        return self::unpad_string($result, $padding);
    }

    public static function hash($data, $key)
    {
        return hash_hmac('sha256', $data, $key);
    }

    protected static function pad_string($data, $block_size, $padding = self::PKCS7_PADDING)
    {
        if ($padding == self::NO_PADDING) {
            return $data;
        }

        $rest = strlen($data) % $block_size;

        if ($rest === 0 && $padding === self::ZERO_PADDING) {
            return $data;
        }

        $pad = $block_size - $rest;
        $char = $padding === self::PKCS7_PADDING ? chr($pad) : chr(0);

        return $data . str_repeat($char, $block_size - $rest);
    }

    protected static function unpad_string($data, $padding = self::PKCS7_PADDING)
    {
        if ($padding === self::NO_PADDING) {
            return $data;
        }

        if ($padding === self::AUTODETECT_PADDING) {
            if (substr($data, -1) === chr(0)) {
                $padding = self::ZERO_PADDING;
            } else {
                $padding = self::PKCS7_PADDING;
            }
        }

        if ($padding !== self::PKCS7_PADDING) {
            return rtrim($data, chr(0));
        }

        $pad = ord(substr($data, -1));

        return substr($data, 0, -$pad);
    }


    protected static function cipher_block_size($method)
    {
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($method));

        return strlen(openssl_encrypt('', $method, '', OPENSSL_RAW_DATA, $iv));
    }

}
