<?php

namespace vams\adminapiclient;

use vams\adminapiclient\exceptions\BadResponseFormatException;
use vams\adminapiclient\security\Crypt;

class ApiClient
{

    const CODE_VERSION = 3;

    /** @var array */
    private $config = [
        'endpoint' => '',
        'timeout' => 30,
        'api_key' => '',
        'api_sign_secret' => '',
        'api_crypt_secret' => '',
        'verify_ssl' => true,
        'ssl_certs_bundle' => null,
        'user_agent' => 'VAMS ADMIN API CLIENT',
    ];

    public function __construct(array $args = [])
    {
        $this->init_config($args);
    }

    // Public methods

    /**
     * @param string $path
     * @param array $args
     * @return ApiResponse
     */
    public function get($path, array $args = [])
    {
        return $this->call($path, $args, 'get');
    }

    /**
     * @param string $path
     * @param array $args
     * @return ApiResponse
     */
    public function post($path, array $args = [])
    {
        return $this->call($path, $args, 'post');
    }

    // Not public methods

    protected function init_config(array $args)
    {
        $this->config['ssl_certs_bundle'] = __DIR__ . DIRECTORY_SEPARATOR . 'assets' . DIRECTORY_SEPARATOR . 'ca-bundle.pem';

        $this->config = array_merge(
            $this->config, array_intersect_key($args, $this->config)
        );

        $this->config['endpoint'] = rtrim($this->config['endpoint'], '/');
    }

    protected function call($path, array $args = [], $http_method = 'get')
    {
        if (empty($path)) {
            $path == '/';
        } elseif ($path[0] !== '/') {
            $path = '/' . $path;
        }

        $response_text = $this->do_request($path, $args, $http_method);
        $response = $this->parse_response($response_text);

        return $response;
    }

    protected function do_request($path, array $args, $http_method)
    {
        $curl = $this->init_curl($path, $args, $http_method);
        $response = curl_exec($curl);

        // request failed?
        if ($response === false) {
            $error = curl_error($curl);
            $errno = curl_errno($curl);
            curl_close($curl);

            throw new ApiException($error, $errno);
        }

        curl_close($curl);

        return $response;
    }

    private function init_curl($path, $args, $http_method)
    {
        $url = $this->config['endpoint'] . $path;
        $curl = curl_init();
        $encrypted_args = Crypt::encrypt(json_encode($args), base64_decode($this->config['api_crypt_secret']), 0, null, Crypt::ZERO_PADDING_EMPTY_BLOCK);
        $params = ['data' => $encrypted_args];

        /* Options */
        curl_setopt($curl, CURLOPT_USERAGENT, $this->config['user_agent']);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $this->config['timeout']);
        curl_setopt($curl, CURLOPT_TIMEOUT, $this->config['timeout']);
        curl_setopt($curl, CURLOPT_HEADER, TRUE);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, $this->config['verify_ssl']); // TRUE for production
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, $this->config['verify_ssl'] ? 2 : 0); // 2 for production
        curl_setopt($curl, CURLOPT_CAINFO, $this->config['ssl_certs_bundle']);

        /* Headers */
        $headers = [
            'x-auth-token:' . $this->calculate_token($path, $encrypted_args, $http_method),
        ];

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        /* Params */
        switch (strtolower($http_method)) {
            case 'get':
                $url = $url . '?' . http_build_query($params);
                break;
            case 'post':
                curl_setopt($curl, CURLOPT_POST, TRUE);
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
                break;
        }

        curl_setopt($curl, CURLOPT_URL, $url);

        return $curl;
    }

    /**
     * Process API results to specified return format
     */
    protected function parse_response($response_text)
    {
        $response = ApiResponse::create_from_text($response_text);

        if (!$response->headers->has('Content-Type')) {
            throw new BadResponseFormatException($response_text, 'Missing Content-Type header');
        }

        $content_type = $response->headers->get('Content-Type');
        $content_type = strstr($content_type, ';', true) ?: $content_type;

        if ($content_type === 'application/vnd.vams.api.json+aes256') {
            $data = Crypt::decrypt($response->get_content(), base64_decode($this->config['api_crypt_secret']), 0, null, Crypt::ZERO_PADDING);
            $response->set_response($this->json_decode($data));
        } elseif ($content_type === 'application/json') {
            $response->set_response($this->json_decode($response->get_content()));
        } else {
            throw new BadResponseFormatException($response_text, 'Unknown Content-Type "' . $content_type . '"');
        }

        return $response;
    }

    protected function json_decode($data)
    {
        $result = @json_decode($data, true);

        if (json_last_error()) {
            throw new BadResponseFormatException(
                $data, function_exists('json_last_error_msg') ? json_last_error_msg() : 'Error parsing response', json_last_error()
            );
        }

        return $result;
    }

    protected function calculate_token($path, $encrypted_args, $http_method)
    {
        $ts = time();
        $key = Crypt::hash($this->config['api_sign_secret'], $ts);
        $signature = Crypt::hash(strtoupper($http_method) . ' ' . $path . '?' . $encrypted_args, $key);

        return
            $this->config['api_key'] . '#' .
            $ts . '#' .
            $signature;
    }

}
