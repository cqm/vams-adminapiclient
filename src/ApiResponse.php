<?php

namespace vams\adminapiclient;

class ApiResponse implements \ArrayAccess, \IteratorAggregate, \Countable
{

    private $status_code;
    /** @var ApiResponseHeaderBag */
    public $headers;
    private $content;
    /** @var array */
    private $response;

    function __construct($status_code, ApiResponseHeaderBag $headers, $content, array $response = [])
    {
        $this->status_code = (int)$status_code;
        $this->headers = $headers;
        $this->content = $content;
        $this->response = $response;
    }

    public static function create_from_text($text)
    {
		$aux = $text;
		do {
			list($header, $content) = explode("\r\n\r\n", $aux, 2);

			if (!preg_match('#^HTTP/[^ ]+ (\d+)#', $header, $matches)) {
				throw new exceptions\BadResponseFormatException($text, 'Missing HTTP declaration');
			}
			$status_code = $matches[1];
			$aux = $content;
		} while ($status_code == 100);

        $headers = new ApiResponseHeaderBag();
        if (preg_match_all("#^(.+): (.+)$#m", str_replace("\r\n", "\n", $header), $matches)) {
            for ($i = 0, $c = count($matches[0]); $i < $c; ++$i) {
                $headers->set($matches[1][$i], $matches[2][$i]);
            }
        }

        return new self($status_code, $headers, $content);
    }

    public function get_response()
    {
        return $this->response;
    }

    public function get_response_code()
    {
        return array_key_exists('code', $this->response) ? $this->response['code'] : null;
    }

    public function get_response_message()
    {
        return array_key_exists('message', $this->response) ? $this->response['message'] : null;
    }

    public function get_response_data()
    {
        return array_key_exists('data', $this->response) ? $this->response['data'] : [];
    }

    public function get_content()
    {
        return $this->content;
    }

    public function set_response(array $response)
    {
        $this->response = $response;
    }

    public function set_response_code($code)
    {
        $this->response['code'] = $code;
    }

    public function set_response_message($message)
    {
        $this->response['message'] = $message;
    }

    public function set_response_data(array $data)
    {
        $this->response['data'] = $data;
    }

    public function set_content($content)
    {
        $this->body = $body;
    }

    public function count()
    {
        return count($this->get_response_data());
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->get_response_data());
    }

    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->get_response_data());
    }

    public function offsetGet($offset)
    {
        if (!$this->offsetExists($offset)) {
            throw new \OutOfBoundsException('Offset not found: '. $offset);
        }

        return $this->response['data'][$offset];
    }

    public function offsetSet($offset, $value)
    {
        $this->response['data'][$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->response['data'][$offset]);
    }

}
