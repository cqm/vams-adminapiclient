<?php

namespace vams\adminapiclient\exceptions;

class BadResponseFormatException extends \vams\adminapiclient\ApiException {

    private $response_text;

    public function __construct($response_text, $message='', $code=0, $previous=null)
    {
        parent::__construct($message, $code, $previous);

        $this->response_text = $response_text;
    }

    public function get_response_text()
    {
        return $this->response_text;
    }

}
